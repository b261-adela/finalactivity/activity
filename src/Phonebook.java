import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact> contacts;

    public Phonebook(){};

    public Phonebook(ArrayList<Contact> contacts){
        this.contacts= contacts;
    }

    //Getter and setter
    public ArrayList<Contact> getContacts(){
        return this.contacts;
    }

    public void setContacts(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

}
